# p!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Ivan Vladimir Meza Ruiz 2022
# GPL 3.0


import sys
import os.path
import requests
from requests.exceptions import ConnectTimeout, ReadTimeout
import parsel
import re
import csv
import random
import time
from datetime import datetime, timezone
from copy import deepcopy
from tinydb import TinyDB, Query
from collections import Counter, defaultdict
from enum import Enum
from fuzzywuzzy import fuzz
from stop_words import get_stop_words  # Lista de palabras funcionales
from multiprocessing import Pool
import subprocess
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError,
)
from PIL import Image
import pytesseract
import tempfile
from functools import partial
import json
from utils import *

SW_DEFAULT = [":", ",", "•", "p.", "-", "si", '"', "'", ")", "(", "~", "/"]

def open_text_file(
    r,
    SW=[],
    lower=False,
    n=3,
    cut_off=0,
    character_level=False,
    remove_first_characters=1000,
):
    if os.path.exists(r["text_filename"]):
        with open(r["text_filename"]) as f:
            content = f.read()[remove_first_characters:]
            content = content.lower() if lower else content
            content = re.sub(r"[^a-zA-Z0-9\sáéíóúñÑÁÉÍÓÚ]", " ", content)
            content = re.sub(r"\s+", " ", content)
            if not character_level:
                content = [w for w in content.split() if not w in SW][50:]
            ngrams = zip(*[content[i:] for i in range(n)])
            ngrams = [" ".join(ngram) for ngram in ngrams]
            ngrams = Counter(ngrams)
            return Counter({k: v for k, v in ngrams.items() if v >= cut_off})


def compare_texts_(
    d1,
    r2,
    SW=[],
    lower=False,
    n=3,
    cut_off=0,
    character_level=False,
    remove_first_characters=1000,
):
    if not r2["doc_number"]:
        return 0.0
    d2 = open_text_file(
        r2,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
        remove_first_characters=remove_first_characters,
    )
    if not d2:
        return 0.0
    if not d1:
        return 0.0
    if len(d2) < 100:
        return 0.0
    return sum((d1 & d2).values()) / sum((d1 | d2).values())


def same_voca_(
    d1,
    r2,
    SW=[],
    lower=False,
    n=3,
    cut_off=0,
    character_level=False,
    remove_first_characters=1000,
):
    if not r2["doc_number"]:
        return 0.0
    d2 = open_text_file(
        r2,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
        remove_first_characters=remove_first_characters,
    )
    if not d2:
        return 0.0
    if not d1:
        return 0.0
    return len(d1 & d2)


def compare_texts(
    r1,
    r2,
    SW=[],
    lower=False,
    n=3,
    cut_off=0,
    character_level=False,
    remove_first_characters=1000,
):
    if not r1["doc_number"]:
        return 0.0
    d1 = open_text_file(
        r1,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
        remove_first_characters=remove_first_characters,
    )
    if len(d1) < 100:
        return 0.0
    return compare_texts_(
        d1,
        r2,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
    )


def same_voca(
    r1,
    r2,
    SW=[],
    lower=False,
    n=3,
    cut_off=0,
    character_level=False,
    remove_first_characters=1000,
):
    if not r1["doc_number"]:
        return 0.0
    d1 = open_text_file(
        r1,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
        remove_first_characters=remove_first_characters,
    )
    return same_voca_(
        d1,
        r2,
        SW=SW,
        lower=lower,
        n=n,
        cut_off=cut_off,
        character_level=character_level,
        remove_first_characters=remove_first_characters,
    )


