# p!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Ivan Vladimir Meza Ruiz 2022
# GPL 3.0


from click_option_group import optgroup
from rich import print
from rich.progress import track
import rich_click as click
from rich.progress import Progress
import sys
import configparser
import os.path
import requests
from requests.exceptions import ConnectTimeout, ReadTimeout
import parsel
import re
import csv
import random
import time
from datetime import datetime, timezone
from copy import deepcopy
from tinydb import TinyDB, Query
from collections import Counter, defaultdict
from enum import Enum
from fuzzywuzzy import fuzz
from stop_words import get_stop_words  # Lista de palabras funcionales
from multiprocessing import Pool
import subprocess
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError,
)
from PIL import Image
import pytesseract
import tempfile
from functools import partial
import json
from utils import *

# TODO: Add other strict search
class Option(Enum):
    asesor = "WASE"
    escuela = "WPOS"


DEFAULT_SECTION = "DEFAULT"
DEFAULTS = {
    "database_filename": "theses.tinydb.json",
    "data_dir": "theses",
    "query": '"Facultad de Ciencias"',
    "option": "escuela",
    "number_counts": 20,
    "sleep_seconds": 5,
    "ini_year": 1950,
    "fin_year": 2023,
    "remove_first_characters": 1000,
    "insert_only_new": True,
    "max": 0,
    "ngram": 3,
    "njobs": 4,
    "cut_off": 0,
    "character_level": False,
    "same_voca": False,
    "ratio": 78,
    "threshold": 0.03,
    "reload": False,
    "lower": False,
    "fill_info": True,
    "output": "output.csv",
    "json_file": "comparison_results.json",
}

HEADER = {
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    "Accept-Language": "en-GB,es-MX;q=0.7,en;q=0.3",
    "Upgrade-Insecure-Requests": "1",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "cross-site",
    "Sec-Fetch-User": "?1",
    "Sec-GPC": "1",
}


def get_config(ctx, args):
    try:
        config = dict(deepcopy(ctx.obj["config"][ctx.obj["config_section"]]))
    except NoneType:
        config = {}

    for k, v in config.items():
        if v:
            if k in [
                "ratio",
                "sleep_seconds",
                "max",
                "ngram",
                "njobs",
                "number_counts",
                "ini_year",
                "fin_year",
                "cut_off",
                "remove_first_characters",
            ]:
                config[k] = int(v)
            elif k in ["threshold"]:
                config[k] = float(v)
            elif k in [
                "insert_only_new",
                "random",
                "lower",
                "character_level",
                "same_voca",
                "fill_info"
            ]:
                config[k] = bool(eval(v))
            elif k in ["option"]:
                if v == "asesor":
                    config[k] = Option.asesor
                if v == "escuela":
                    config[k] = Option.escuela

    for k, v in args.items():
        if v:
            if k in [
                "ratio",
                "sleep_seconds",
                "max",
                "ngram",
                "njobs," "number_counts",
                "ini_year",
                "fin_year",
                "cut_off",
                "remove_first_characters",
            ]:
                config[k] = int(v)
            elif k in ["threshold"]:
                config[k] = float(v)
            elif k in [
                "insert_only_new",
                "random",
                "lower",
                "character_level",
                "same_voca",
                "fill_info"
            ]:
                config[k] = bool(v)
            elif k in ["option"]:
                if v == "asesor":
                    config[k] = Option.asesor
                if v == "escuela":
                    config[k] = Option.escuela
     
            else:
                config[k] = v
    return config



@click.group()
@click.option("--config-filename", type=click.Path(), default="config.ini")
@click.option("--config-section", type=str, default=DEFAULT_SECTION)
@click.option("-v", "--verbose", is_flag=True, help="Verbose mode")
@click.pass_context
def tesis_analysis(ctx, config_filename, config_section, verbose=False):
    ctx.ensure_object(dict)
    config = configparser.ConfigParser(DEFAULTS)

    if os.path.exists(config_filename):
        config.read(config_filename)
    else:
        print(
            f"[yellow]Warning '{config_filename}' not found found; using default config[/] [white bold]: {config}[/]"
        )
    if not config_section in config:
        print(f"[red]Error '{config_section}' section not found[/]")
        sys.exit(100)

    ctx.obj["config"] = config
    ctx.obj["config_section"] = config_section
    ctx.obj["verbose"] = verbose


@tesis_analysis.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--ini_year", type=int, help="Initial year")
@click.option("--fin_year", type=int, help="Final year")
@click.pass_context
def list_records(
    ctx,
    **args,
):
    """Print newspapers"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    for r in db:
        if (
            int(r["year"]) >= config["ini_year"]
            and int(r["year"]) <= config["fin_year"]
        ):
            print(r)
    print(f"[green]Total: {len(db)}[/]")


@tesis_analysis.command()
@click.argument("doc_number", type=str)
@click.option("--database-filename", type=str, help="Database filename")
@click.pass_context
def delete_record(
    ctx,
    doc_number,
    **args,
):
    """Deletes a record by number of document"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    T = Query()
    db.remove(T.doc_number == doc_number)
    print(f"[green]Total: {len(db)}[/]")


@tesis_analysis.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--ini_year", type=int, help="Initial year")
@click.option("--fin_year", type=int, help="Final year")
@click.option("--ratio", type=int, help="Ratio of similarity")
@click.pass_context
def similar_title(
    ctx,
    **args,
):
    """Prints all the records"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    titles = defaultdict(list)
    ignore = set()
    for r in track(db):
        if (
            r["doc_number"]
            and int(r["year"]) >= config["ini_year"]
            and int(r["year"]) <= config["fin_year"]
        ):
            l = titles[r["title"].lower().strip()]
            l.append(r)
    same = [rs for _, rs in titles.items() if len(rs) > 1]
    print(f"[blue]{' Same title'.rjust(80,'▬')}[/]")
    for i, rs in enumerate(same):
        print(f"[red]{i+1} - same title ({len(rs)})[/]")
        for i, r in enumerate(rs):
            print(f"[yellow]    {i+1} - {r['doc_number']} - {r['text_filename']}[/]")
    similar = defaultdict(list)
    titles = [(r["title"], r) for r in db]
    for i, (t1, r1) in track(enumerate(titles)):
        if not r1["doc_number"]:
            continue
        if r1["num"] in ignore:
            continue
        similar[t1.lower()].append((r1, 1.0))
        for t2, r2 in titles[i + 1 :]:
            if not r2["doc_number"]:
                continue
            ratio = fuzz.token_sort_ratio(t1.lower(), t2.lower())
            if ratio > config["ratio"]:
                similar[t1.lower()].append((r2, ratio / 100))
                ignore.add(r2["num"])
    print(f"\n[blue]{' Similar title'.rjust(80,'▬')}[/]")
    ix = 0
    for _, rs in similar.items():
        if len(rs) > 1:
            ix += 1
            print(f"[red]{ix} - Found similar title ({len(rs)})[/]")
            r, ratio = rs[0]
            print(f"[cyan]    Pivot: {r['doc_number']} - {r['text_filename']}[/]")
            for i, (r, ratio) in enumerate(rs[1:]):
                print(
                    f"[yellow]    {i+1} {r['doc_number']} - {r['text_filename']} - {ratio}[/]"
                )


@tesis_analysis.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--json_file", type=str, help="JSON file for results")
@click.option("--lower", is_flag=True, help="Lower all text")
@click.option("--same_voca", is_flag=True, help="Lower all text")
@click.option("--ini_year", type=int, help="Initial year")
@click.option("--fin_year", type=int, help="Final year")
@click.option("--ngram", type=int, help="Ngram size")
@click.option("--cut_off", type=int, help="Cut off counts")
@click.option("--character_level", is_flag=True, help="ngrams to the character level")
@click.option("--threshold", type=float, help="Threshold of similarity")
@click.option(
    "--remove_first_characters",
    type=int,
    help="Number of characters to remove from the benining",
)
@click.option("--njobs", type=int, help="Number of jobs")
@click.pass_context
def similar_content(
    ctx,
    **args,
):
    """Looks for similar documents"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    T = Query()
    titles = defaultdict(list)
    documents = {}
    SW = list(SW_DEFAULT)
    SW += get_stop_words("es")
    similar = defaultdict(list)
    titles = [
        (r["title"], r)
        for r in db
        if r["doc_number"]
        and int(r["year"]) >= config["ini_year"]
        and int(r["year"]) <= config["fin_year"]
        and "text_filename" in r
        and os.path.exists(r["text_filename"])
    ]
    ignore = set()
    pool = Pool(processes=config["njobs"])
    print(f"\n[blue]{' Comparing'.rjust(80,'▬')}[/]")
    func = same_voca_ if config["same_voca"] else compare_texts_
    results = {}
    for i, (t1, r1) in track(enumerate(titles)):
        print(f"[blue]Procesing ({i+1}/{len(titles)}): {r1['title']}[/]")
        if not r1["doc_number"]:
            continue
        ##if r1["num"] in ignore:
        ##    continue
        d1 = open_text_file(
            r1,
            SW=SW,
            lower=config["lower"],
            n=config["ngram"],
            cut_off=config["cut_off"],
            character_level=config["character_level"],
            remove_first_characters=config["remove_first_characters"],
        )
        results[r1["doc_number"]] = {}
        similar[t1.lower()].append((r1, 1.0))
        for ((t2, r2), ratio) in zip(
            titles[i + 1 :],
            pool.starmap(
                func,
                [
                    (
                        d1,
                        r2,
                        SW,
                        config["lower"],
                        config["ngram"],
                        config["cut_off"],
                        config["character_level"],
                        config["remove_first_characters"],
                    )
                    for (_, r2) in titles[i + 1 :]
                ],
            ),
        ):
            results[r1["doc_number"]][r2["doc_number"]] = ratio
            if ratio >= config["threshold"]:
                similar[t1.lower()].append((r2, ratio))
                ##ignore.add(r2["num"])

    print(f"\n[blue]{' Similar content'.rjust(80,'▬')}[/]")
    ix = 0
    for _, rs in similar.items():
        if len(rs) > 1:
            ix += 1
            print(f"[red]{ix} - Found similar title ({len(rs)})[/]")
            r, ratio = rs[0]
            print(f"[cyan]Pivot: {r['doc_number']} - {r['text_filename']}[/]")
            for i, (r, ratio) in enumerate(rs[1:]):
                print(
                    f"[yellow]Found: {r['doc_number']} - {r['text_filename']} - {ratio}[/]"
                )
    json_object = json.dumps(results, indent=4)
    with open(config["json_file"], "w") as outfile:
        outfile.write(json_object)


@tesis_analysis.command()
@click.argument("pivot_doc", type=str)
@click.argument("document", nargs=-1, type=str)
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--lower", is_flag=True, default=False, help="Lower all text")
@click.option("--same_voca", is_flag=True, help="Lower all text")
@click.option("--ngram", type=int, help="Ngram size")
@click.option("--cut_off", type=int, help="Cut off counts")
@click.option("--character_level", is_flag=True, help="ngrams to the character level")
@click.option(
    "--remove_first_characters",
    type=int,
    help="Number of characters to remove from the benining",
)
@click.option("--number_counts", type=int, help="Number of counts to display")
@click.pass_context
def compare_theses(
    ctx,
    pivot_doc,
    document,
    **args,
):
    """Compares two docuements"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    titles = defaultdict(list)
    documents = {}
    SW = list(SW_DEFAULT)
    SW += get_stop_words("es")
    T = Query()
    r1 = db.search(T.doc_number == pivot_doc)[0]
    d1 = open_text_file(
        r1,
        SW=SW,
        lower=config["lower"],
        n=config["ngram"],
        cut_off=config["cut_off"],
        character_level=config["character_level"],
        remove_first_characters=config["remove_first_characters"],
    )
    for doc in document:
        r2 = db.search(T.doc_number == doc)[0]
        if config["same_voca"]:
            res = same_voca(
                r1,
                r2,
                SW=SW,
                lower=config["lower"],
                n=config["ngram"],
                cut_off=config["cut_off"],
                character_level=config["character_level"],
                remove_first_characters=config["remove_first_characters"],
            )
        else:
            res = compare_texts(
                r1,
                r2,
                SW=SW,
                lower=config["lower"],
                n=config["ngram"],
                cut_off=config["cut_off"],
                character_level=config["character_level"],
                remove_first_characters=config["remove_first_characters"],
            )
        print(f"[green]Similarity: {res}[/]")
        d2 = open_text_file(
            r2,
            SW=SW,
            lower=config["lower"],
            n=config["ngram"],
            cut_off=config["cut_off"],
            character_level=config["character_level"],
            remove_first_characters=config["remove_first_characters"],
        )
        print(f"[blue]Fist file  : {r1['text_filename']}[/]")
        print(
            ", ".join([f"{k}:{v}" for k, v in d1.most_common(config["number_counts"])]),
            len(d1),
        )
        print(f"[blue]Second file: {r2['text_filename']}[/]")
        print(
            ", ".join([f"{k}:{v}" for k, v in d2.most_common(config["number_counts"])]),
            len(d2),
        )
        print(f"[blue]Same vocabulary[/]")
        print(
            ", ".join(
                [f"{k}:{v}" for k, v in (d1 & d2).most_common(config["number_counts"])]
            ),
            len(d2 & d1),
        )


re_pdf_url = re.compile(
    r"(?P<prefix>\w+)(?P<year>\d+)/(?P<month>\w+)?/(?P<doc_number>\d+)/(?P<pdf_name>.+)\.pdf"
)


def ocr_pdf_file(pdf_file, text_file, njobs=4):
    with open(text_file, "w") as f:
        with tempfile.TemporaryDirectory() as path:
            images = convert_from_path(pdf_file, output_folder=path)
            for text_page in map(
                partial(pytesseract.image_to_string, **{"lang": "spa"}), images
            ):
                f.write(text_page)


@tesis_analysis.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--data-dir", type=str, help="Data directory for txt")
@click.option("--reload", is_flag=True, default=False, help="Download files again")
@click.option("--njobs", type=int, help="Number of jobs")
@click.pass_context
def download_pdf_populate_text(
    ctx,
    **args,
):
    """Print newspapers"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"])
    headers = dict(HEADER)
    T = Query()
    files_to_ocr = []
    for r in track(db):
        # url=f"http://132.248.9.195/ptd{r['year']}/{r['doc_number'][2:]}/Index.html"
        if not r["doc_number"]:
            continue
        if not "pdf_url" in r:
            url = f"https://tesiunam.dgb.unam.mx:443/F/?func=service&doc_library=TES01&doc_number={r['doc_number']}&line_number=0001&func_code=WEB-FULL&service_type=MEDIA"
            r_ = requests.get(url, headers=headers)
            sel = parsel.Selector(text=r_.text)
            url = sel.xpath("body/@onload").get()
            strings = re_strings.findall(url)
            pdf_url = strings[0][1:-1]
            r_ = requests.get(pdf_url, headers=headers)
            sel = parsel.Selector(text=r_.text)
            url = sel.xpath('//frame[@name="principal"]/@src').get()
            try:
                pdf_url = url.split("=")[1].replace("http://132.248.9.195", "")
            except IndexError:
                pdf_url = pdf_url.replace("Index.html", url).replace(
                    "http://132.248.9.195", ""
                )
        else:
            pdf_url = r["pdf_url"].replace("http://132.248.9.195", "")

        # Fixing minor error found there
        url = f"http://132.248.9.195{pdf_url}".replace("/195/", "/")
        r_ = requests.get(url, stream=True)
        pdf_file = os.path.join(
            config["data_dir"], f"{r['year']}-{r['title']}-{r['author']}.pdf"
        ).replace('"', "")
        text_file = os.path.join(
            config["data_dir"], f"{r['year']}-{r['title']}-{r['author']}.txt"
        ).replace('"', "")
        if not os.path.exists(pdf_file):
            with open(pdf_file, "wb") as fd:
                for chunk in r_.iter_content(2000):
                    fd.write(chunk)
        if not os.path.exists(text_file):
            ocr_pdf_file(pdf_file, text_file)
        dt = datetime.now(timezone.utc)
        db.update(
            {
                "pdf_url": url,
                "pdf_filename": pdf_file,
                "text_filename": text_file,
                "date_updated": str(dt),
            },
            T.doc_number == r["doc_number"],
        )

    print(f"[green]Total: {len(db)}[/]")


re_tesis = re.compile(r"doc_number=(\d+)&")
re_strings = re.compile(r"'.*?'(?<!\\')")
re_link = re.compile(r"'<A HREF=([^>]*>)")


@tesis_analysis.command()
@click.option("--database-filename", type=str, help="Database filename")
@click.option("--query", type=str, help="Query to pass")
@click.option("--option", type=str, help="Option for the query")
@click.option("--sleep_seconds", type=int, help="Seconds to await between request")
@click.option("--insert_only_new", type=bool, help="Insert only new newspaper entries")
@click.option("--max", type=int, help="Maximum number of sources to analyse")
@click.option("--fill_info", type=bool, help="Fill info from thesis information")
@click.pass_context
def download_query(
    ctx,
    **args,
):
    """Explore a query to identify the right id to extract later"""
    config = get_config(ctx, args)
    db = TinyDB(config["database_filename"],encoding="utf-8")
    T = Query()
    headers = dict(HEADER)

    url = f'https://tesiunam.dgb.unam.mx/F/?func=find-b&find_code={config["option"].value}&request={"+".join(config["query"].split())}'
    print(f"[cyan]URL: {url}[/]")
    theses = []
    next_link = True
    print(f"[cyan]Extracting documents {url}[/]")
    stats = Counter()
    break_max=False
    while next_link:
        r = requests.get(url, headers=headers)
        r.encoding = r.apparent_encoding
        sel = parsel.Selector(text=r.text)

        for i, row in enumerate(sel.xpath("//tr")):
            if i == 0:
                continue
            stats["row"] += 1
            cols = row.xpath("td")
            num = cols[0].xpath("a/text()").get()
            name = cols[2].xpath("text()").get()
            script = cols[3].xpath("script/text()").get()
            strings = re_strings.findall(script)
            year = cols[4].xpath("text()").get()
            title = strings[0][1:-2].strip().replace("&nbsp;", " ")
            link = cols[5].xpath("a/@href").get()
            info = row.xpath("tr")

            if link:
                m = re_tesis.search(link)
                if m:
                    stats["link"] += 1
                    theses.append((m.group(1), name, title, year))
            else:
                theses.append((None, name, title, year))
                stats["no_link"] += 1

            link_info = re_link.search(script).group(1)
            info={}
            if config['fill_info'] and link_info:
                r_ = requests.get(link_info, headers=headers)
                r_.encoding = r_.apparent_encoding
                sel_ = parsel.Selector(text=r_.text)

                section_= sel_.xpath("//section[contains(@id,'formato-completo')]")[0]

                for i, row_ in enumerate(section_.xpath("//tr")):
                    key=row_.xpath("th/text()").get().replace("\n","").strip()
                    value=row_.xpath("td/text()").get().replace("\n","").strip()
                    if len(value)==0:
                        value=row_.xpath("td/a/text()").get()
                        if value:
                            value.replace('\n',"").strip()
                        else:
                            value=""
                    info[key]=value

            dt = datetime.now(timezone.utc)
            record={
                    "date_created": str(dt),
                    "date_updated": str(dt),
                    "query": config["query"],
                    "num": num,
                    "title": title,
                    "author": name,
                    "year": year,
                    "doc_number": theses[-1][0],
                }
            if len(info)>0:
                record['info']=info

            db.insert(record)

            if stats['link']>config['max']:
                break_max=True
                break
        if break_max:
            break

        url = sel.xpath("//a[@title='Next']/@href").get()
        next_link = True if url else False
    print(f'[green]Total rows {stats["row"]}[/]')
    print(f'[green]Total rows {stats["link"]}[/]')
    print(f'[green]Total rows {stats["no_link"]}[/]')


if __name__ == "__main__":
    tesis_analysis(obj={})
