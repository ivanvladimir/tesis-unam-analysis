
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)

# Tesis UNAM analysis

CLI to extract and analysis tesis from UNAM repository


## Dependencies

It requieres the software _tesseract.ocr_ for  linux:
install:

    sudo apt install tesseract-ocr
    sudo apt install tesseract-ocr-spa

These are the libraries used

* click==8.1.3
* click-option-group==0.5.5
* parsel==1.7.0
* requests==2.28.1
* rich-click==1.6.0
* tinydb==4.7.0
* fuzzywuzzy==0.18.0
* pytesseract==0.3.10
* pdf2image==1.16.2
* rapidfuzz==2.13.7
* stop-words==2018.7.23


## Install

To install:

    python -m venv env
    source env/bin/activate
    pip install -r requirements.txt

## Execute

### Extract theses given a query

__Right now only looks for queries in the asesor field__


Given a query fills the database with general information about the query

    python thesis-analysis.py download-query  

Example for a specific query and database

    python thesis-analysis.py download-query --database-filename ivanmeza.tinydb.json --query 'Meza, I.'


### List  records

List the downloaded records
    
    python thesis-analysis.py list-records

Example for a specific database

    python thesis-analysis.py list-records --database-filename ivanmeza.tinydb.json


### Delete record

Delete a rexord
    
    python thesis-analysis.py delete-record DOC_NUMBER

Example for a specific database

    python thesis-analysis.py delete-record --database-filename ivanmeza.tinydb.json "000236259"


### Populates the pdf and text files

Given the records in a local database fills the information

    python thesis-analysis.py download-pdf-populate-text 

Example for a specific database

    python thesis-analysis.py download-pdf-populate-text --database-filename ivanmeza.tinydb.json


### List similar titles

List groups of theses with similar titles

    python thesis-analysis.py similar-title

Example for a specific database

    python thesis-analysis.py similar-title --database-filename ivanmeza.tinydb.json --ratio 60

### List similar content

List groups of theses with similar content

    python thesis-analysis.py similar-content 

Example for a specific database

     python thesis-analysis.py similar-content --database-filename ivanmeza.tinydb.json --threshold 0.05 --ngram 2 --njobs 8

Example two comapare specific documents database by n-grams

    python thesis-analysis.py similar-content --database-filename ivanmeza.tinydb.json --threshold 100 --ngram 10 --json_file data/ivanmeza.comparison_results.json --remove_first_characters 1000 --same_voca

### Compare two documents

Compare two records

    python thesis-analysis.py compare-theses 000049842 000033882


## More documentation

For further documentation check [this post](https://turing.iimas.unam.mx/~ivanvladimir/posts/network-plagarism/) in Spanish
